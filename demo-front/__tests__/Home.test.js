import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom'
import Home from '../pages/index';
import { createNotification, fetchNotifications } from '../services/api';

jest.mock('../services/api');

// Basic render test
test('renders the Home component with all main elements', () => {
  render(<Home baseUrl="http://localhost:3000" />);
  expect(screen.getByText('Form')).toBeInTheDocument();
  expect(screen.getByText('Notifications')).toBeInTheDocument();
  expect(screen.getByText('Category')).toBeInTheDocument();
  expect(screen.getByText('Send')).toBeInTheDocument();
});

// Conditional rendering test
test('displays "There is no notifications yet." when there are no notifications', () => {
  render(<Home baseUrl="http://localhost:3000" />);
  expect(screen.getByText('There is no notifications yet.')).toBeInTheDocument();
});
