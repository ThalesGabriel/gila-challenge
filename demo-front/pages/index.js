import { useState } from 'react';
import {
  Box,
  Button,
  FormControl,
  InputLabel,
  List,
  MenuItem,
  Select,
  TextField,
  Typography,
  IconButton,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Grid
} from '@mui/material';
import RefreshIcon from '@mui/icons-material/Refresh';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { fetchNotifications, createNotification } from '../services/api';

const parseDate = (instant) => {
  const date = new Date(instant);
  return date.toLocaleString()
};

const notificationComponent = (index, notification) =>
  <Accordion key={index}>
    <AccordionSummary expandIcon={<ExpandMoreIcon />}>
      <Typography>
        Notification sent to {notification.user.name} at {parseDate(notification.createdAt)}
      </Typography>
    </AccordionSummary>
    <AccordionDetails>
      <Box sx={{ flexDirection: 'row', width: '100%' }}>
        <Typography>
          <strong>Id:</strong> {notification.notificationId}
        </Typography>
        <Typography>
          <strong>Categories:</strong> {notification.notificationRequest.categories.join(", ")}
        </Typography>
        <Typography>
          <strong>Message:</strong> {notification.notificationRequest.message || 'N/A'}
        </Typography>
        <Typography>
          <strong>Type:</strong> {notification.type || 'N/A'}
        </Typography>
        <Typography>
          <strong>User:</strong> {notification.user.id || 'N/A'}
        </Typography>
        <Typography>
          <strong>CreatedAt:</strong> {notification.createdAt || 'N/A'}
        </Typography>
        <Typography>
          <strong>Status:</strong> {notification.status || 'N/A'}
        </Typography>
      </Box>
    </AccordionDetails>
  </Accordion>;

export const getStaticProps = async () => {
  return { props: { baseUrl: process.env.REACT_APP_API_BASE_URL } }
}

export default function Home({ baseUrl }) {
  const [categories, setCategories] = useState([]);
  const [message, setMessage] = useState('');
  const [notifications, setNotifications] = useState([]);
  const [isReloading, setIsReloading] = useState(false);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const newNotification = { categories, message };
    await createNotification(baseUrl, newNotification)
    setCategories([]);
    setMessage('');
    console.log("Please click on the refresh icon to update notifications list")
  };

  const handleReload = async () => {
    setIsReloading(true);
    try {
      const notifications = await fetchNotifications(baseUrl);
      setNotifications(notifications);
      console.log(notifications)
    } catch (error) {
      console.error('Erro ao recarregar notificações:', error);
    } finally {
      setIsReloading(false);
    }
  };

  return (
    <Box
      display="flex"
      height="100vh"
      alignItems="center"
      justifyContent="center"
      padding={2}
    >
      <Grid
        container
        width="100%"
        height="80%"
        borderRadius={2}
        boxShadow={3}
        overflow="auto"
        spacing={2}
      >
        {/* Form Section */}
        <Grid
          item
          display="flex"
          flexDirection="column"
          padding={3}
          sm={12}
          md={6}
        >
          <Box>
            <Box padding={3}>
              <Typography variant="h5" marginBottom={2}>
                Form
              </Typography>
            </Box>
            <form onSubmit={handleSubmit}>
              <FormControl fullWidth margin="normal">
                <InputLabel>Category</InputLabel>
                <Select
                  multiple
                  id="category-select"
                  value={categories}
                  onChange={(e) => setCategories(e.target.value)}
                >
                  {['NEWS', 'SPORTS', 'FINANCE'].map((option) => (
                    <MenuItem key={option} value={option}>
                      {option}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
              <TextField
                label="Message"
                multiline
                rows={4}
                fullWidth
                margin="normal"
                value={message}
                onChange={(e) => setMessage(e.target.value)}
              />
              <Button
                type="submit"
                variant="contained"
                color="primary"
                fullWidth
                margin="normal"
              >
                Send
              </Button>
            </form>
          </Box>
        </Grid>
        {/* <Divider orientation="vertical" flexItem /> */}
        <Grid
          item
          flex={1}
          padding={3}
          sm={12}
          md={6}
        >
          <Box
            flex={1}
            display="flex"
            alignItems={"center"}
            flexDirection="row"
            padding={3}
          >
            <Typography variant="h5">
              Notifications
            </Typography>
            <IconButton color="primary" onClick={handleReload}>
              <RefreshIcon />
            </IconButton>
          </Box>
          <Box overflow={'auto'}>
            <List>
              {notifications.length == 0 ?
                "There is no notifications yet."
                :
                notifications.map((notification, index) => (
                  notificationComponent(index, notification)
                ))
              }
            </List>
          </Box>
        </Grid>
      </Grid>
    </Box >
  );
}
