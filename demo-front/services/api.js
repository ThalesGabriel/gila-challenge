import axios from 'axios';

export const createNotification = async (baseUrl, notificationInput) => {

  try {
    const response = await axios.post(baseUrl + '/notifications', { ...notificationInput });
    return response.data;
  } catch (error) {
    console.error('Erro ao buscar notificações:', error);
    throw error;
  }

};

export const fetchNotifications = async (baseUrl) => {

  try {
    const response = await axios.get(baseUrl + '/notifications');
    return response.data;
  } catch (error) {
    console.error('Erro ao buscar notificações:', error);
    throw error;
  }

};
