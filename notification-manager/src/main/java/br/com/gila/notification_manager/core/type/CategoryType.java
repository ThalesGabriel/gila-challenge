package br.com.gila.notification_manager.core.type;

public enum CategoryType {
    SPORTS, NEWS, FINANCE;
}
