package br.com.gila.notification_manager.infrastructure.adapter.database;


import br.com.gila.notification_manager.core.domain.notification.NotificationDomain;
import br.com.gila.notification_manager.core.gateway.NotificationGateway;
import br.com.gila.notification_manager.domain.notification.Notification;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//@Component
public class GenericNotificationGateway implements NotificationGateway<Notification> {

    private final List<Notification> notificationHistory = new ArrayList<>();

    @Override
    public void persist(NotificationDomain notification) {
        this.notificationHistory.add(Notification.fromEntity(notification));
    }

    @Override
    public List<Notification> retrieve() {
        List<Notification> reversedList = new ArrayList<>(notificationHistory);
        Collections.reverse(reversedList);
        return reversedList;
    }

}
