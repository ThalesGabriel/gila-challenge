package br.com.gila.notification_manager.core.type;

public enum StatusType {
    SENT, NOT_SENT, ERROR;
}
