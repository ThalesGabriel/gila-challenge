package br.com.gila.notification_manager.core.domain;

import java.util.Objects;

public abstract class Entity<Id> {

    protected final Id id;

    protected Entity(Id id) {
        Objects.requireNonNull(id, "'id' should not be null.");
        this.id = id;
    }

    public Id getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Entity<?> entity = (Entity<?>) o;
        return getId().equals(entity.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

}
