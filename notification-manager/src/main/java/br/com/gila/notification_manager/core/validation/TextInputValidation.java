package br.com.gila.notification_manager.core.validation;

public interface TextInputValidation {

    String validateAndSanitizeInput(String input);

}
