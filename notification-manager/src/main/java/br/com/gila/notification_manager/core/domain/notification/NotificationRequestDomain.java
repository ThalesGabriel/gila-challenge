package br.com.gila.notification_manager.core.domain.notification;

import br.com.gila.notification_manager.core.type.CategoryType;

import java.util.List;

public interface NotificationRequestDomain {


    List<CategoryType> getCategories();

    String getMessage();

    void sanitizeMessage(String message);

}
