package br.com.gila.notification_manager.application.channel;

import br.com.gila.notification_manager.core.domain.notification.NotificationRequestDomain;
import br.com.gila.notification_manager.core.gateway.ChannelGateway;
import br.com.gila.notification_manager.core.type.NotificationType;
import br.com.gila.notification_manager.domain.notification.Notification;
import br.com.gila.notification_manager.domain.user.User;

import java.util.List;

public class EmailHandler extends ChannelHandler {

    public EmailHandler(ChannelGateway<Notification> channelGateway) {
        super(channelGateway);
    }

    @Override
    protected boolean canHandle(List<NotificationType> subscribedChannels) {
        return subscribedChannels.contains(NotificationType.EMAIL);
    }

    @Override
    protected void processNotification(NotificationRequestDomain request, User user) {
        System.out.println("Processing EMAIL notification for user: " + user.getName());
        Notification notification = new Notification(NotificationType.EMAIL, user, request);
        notification.validate();
        channelGateway.sendNotification(notification);
    }

}
