package br.com.gila.notification_manager.core.domain.user;

import br.com.gila.notification_manager.core.type.CategoryType;
import br.com.gila.notification_manager.core.type.NotificationType;

import java.util.List;
import java.util.UUID;

public interface UserDomain {

    UUID getId();

    String getPhone();

    String getEmail();

    String getName();

    List<CategoryType> getSubscribedCategories();

    List<NotificationType> getSubscribedChannels();
}
