package br.com.gila.notification_manager.domain.notification;

import br.com.gila.notification_manager.core.domain.Aggregate;
import br.com.gila.notification_manager.core.domain.notification.NotificationDomain;
import br.com.gila.notification_manager.core.domain.notification.NotificationRequestDomain;
import br.com.gila.notification_manager.core.domain.user.UserDomain;
import br.com.gila.notification_manager.core.type.NotificationType;
import br.com.gila.notification_manager.core.type.StatusType;
import br.com.gila.notification_manager.infrastructure.adapter.owasp.OwaspTextInputValidator;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.util.UUID;

public class Notification extends Aggregate<UUID> implements NotificationDomain {

    private final NotificationType type;
    private final UserDomain user;
    private final NotificationRequestDomain notificationRequest;
    private final Instant createdAt;
    private StatusType status;

    protected Notification(UUID notificationId, NotificationType type, UserDomain user, Instant createdAt, StatusType status, NotificationRequestDomain notificationRequest) {
        super(notificationId);
        this.type = type;
        this.user = user;
        this.notificationRequest = notificationRequest;
        this.createdAt = createdAt;
        this.status = status;
    }

    public Notification(NotificationType type, UserDomain user, NotificationRequestDomain notificationRequest) {
        super(UUID.randomUUID());
        this.type = type;
        this.user = user;
        this.notificationRequest = notificationRequest;
        this.createdAt = Instant.now();
        this.status = StatusType.NOT_SENT;
    }

    public static Notification fromEntity(NotificationDomain entity) {
        return new Notification(entity.getId(), entity.getType(), entity.getUser(), entity.getCreatedAt(), entity.getStatus(), entity.getNotificationRequest());

    }

    @Override
    public NotificationRequestDomain getNotificationRequest() {
        return this.notificationRequest;
    }

    @Override
    public NotificationType getType() {
        return type;
    }

    @Override
    public UserDomain getUser() {
        return user;
    }

    @Override
    public Instant getCreatedAt() {
        return createdAt;
    }

    @Override
    public StatusType getStatus() {
        return status;
    }

    public void markMessageAsSent() {
        this.status = StatusType.SENT;
    }

    public void markMessageAsNotSentByError() {
        this.status = StatusType.ERROR;
    }

    public void sanitizeMessage(String message) {
        this.getNotificationRequest().sanitizeMessage(message);
    }

    @Override
    public String toString() {
        return "Notification{" +
                "notificationId=" + this.id +
                ", request=" + notificationRequest +
                ", type=" + type +
                ", user=" + user +
                ", createdAt=" + createdAt +
                ", status=" + status +
                '}';
    }

    @Override
    public void validate() {
        new NotificationValidatorProcessor().validate(this);
    }
}
