package br.com.gila.notification_manager.domain.notification;

import br.com.gila.notification_manager.core.domain.notification.NotificationRequestDomain;
import br.com.gila.notification_manager.core.type.CategoryType;

import java.util.List;

public class NotificationRequest implements NotificationRequestDomain {

    private List<CategoryType> categories;
    private String message;

    public NotificationRequest() {
    }

    public NotificationRequest(List<CategoryType> categories, String message) {
        this.categories = categories;
        this.message = message;
    }

    @Override
    public List<CategoryType> getCategories() {
        return categories;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public void sanitizeMessage(String message) {
        this.message = message;
    }
}
