package br.com.gila.notification_manager.application.channel;

import br.com.gila.notification_manager.core.domain.notification.NotificationRequestDomain;
import br.com.gila.notification_manager.core.gateway.ChannelGateway;
import br.com.gila.notification_manager.core.type.NotificationType;
import br.com.gila.notification_manager.domain.notification.Notification;
import br.com.gila.notification_manager.domain.user.User;

import java.util.List;

public abstract class ChannelHandler {
    protected ChannelGateway<Notification> channelGateway;
    protected ChannelHandler nextHandler;

    public void setNextHandler(ChannelHandler nextHandler) {
        this.nextHandler = nextHandler;
    }

    public ChannelHandler(ChannelGateway<Notification> channelGateway) {
        this.channelGateway = channelGateway;
    }

    public void handle(NotificationRequestDomain notification, User user) {
        if (canHandle(user.getSubscribedChannels())) {
            processNotification(notification, user);
        }

        if (nextHandler != null) {
            nextHandler.handle(notification, user);
        }
    }

    protected abstract boolean canHandle(List<NotificationType> subscribedChannels);

    protected abstract void processNotification(NotificationRequestDomain notification, User user);

}
