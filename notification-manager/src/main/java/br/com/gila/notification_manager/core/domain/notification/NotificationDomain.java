package br.com.gila.notification_manager.core.domain.notification;

import br.com.gila.notification_manager.core.domain.user.UserDomain;
import br.com.gila.notification_manager.core.type.NotificationType;
import br.com.gila.notification_manager.core.type.StatusType;

import java.time.Instant;
import java.util.UUID;

public interface NotificationDomain {
    UUID getId();
    NotificationRequestDomain getNotificationRequest();
    NotificationType getType();
    UserDomain getUser();
    Instant getCreatedAt();
    StatusType getStatus();
}
