package br.com.gila.notification_manager.core.service;

import br.com.gila.notification_manager.core.domain.notification.NotificationDomain;
import br.com.gila.notification_manager.core.domain.notification.NotificationRequestDomain;

import java.util.List;

public interface NotificationService<T extends NotificationDomain> {

    void sendNotification(NotificationRequestDomain input);

    List<T> retrieveNotifications();

}
