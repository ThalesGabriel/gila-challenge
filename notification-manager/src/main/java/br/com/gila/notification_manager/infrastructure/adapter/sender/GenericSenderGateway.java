package br.com.gila.notification_manager.infrastructure.adapter.sender;

import br.com.gila.notification_manager.core.gateway.ChannelGateway;
import br.com.gila.notification_manager.core.gateway.NotificationGateway;
import br.com.gila.notification_manager.domain.notification.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GenericSenderGateway implements ChannelGateway<Notification> {

    @Autowired
    public NotificationGateway<Notification> notificationGateway;

    @Override
    public void sendNotification(Notification notification) {
        System.out.println("Simulation sending a message through an external API.");
        notification.markMessageAsSent();
        notificationGateway.persist(notification);
    }

}
