package br.com.gila.notification_manager.core.domain.notification;

import br.com.gila.notification_manager.core.domain.Validator;
import br.com.gila.notification_manager.domain.notification.Notification;

public abstract class NotificationValidator extends Validator<Notification> { }
