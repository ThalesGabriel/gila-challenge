package br.com.gila.notification_manager.infrastructure.adapter.owasp;

import br.com.gila.notification_manager.core.validation.TextInputValidation;
import jakarta.annotation.PostConstruct;
import org.owasp.validator.html.AntiSamy;
import org.owasp.validator.html.CleanResults;
import org.owasp.validator.html.Policy;
import org.owasp.validator.html.PolicyException;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.regex.Pattern;

public class OwaspTextInputValidator implements TextInputValidation {

    private AntiSamy antiSamy;

    @PostConstruct
    public void initialize() {
        try {
            InputStream inputStream = OwaspTextInputValidator.class.getResourceAsStream("/antisamy.xml");
            Policy policy = Policy.getInstance(inputStream);
            antiSamy = new AntiSamy(policy);
        } catch (PolicyException e) {
            throw new RuntimeException("Failed to initialize AntiSamy", e);
        }
    }

    @Override
    public String validateAndSanitizeInput(String input) {
        // Verificar comprimento
        if (input.length() > 500) {
            throw new IllegalArgumentException("Descrição muito longa");
        }

        // Expressões regulares para verificar padrões proibidos
        String[] forbiddenPatterns = {"<script>", "javascript:"};
        for (String pattern : forbiddenPatterns) {
            if (Pattern.compile(pattern, Pattern.CASE_INSENSITIVE).matcher(input).find()) {
                throw new IllegalArgumentException("Padrão proibido encontrado: " + pattern);
            }
        }

        // Sanitizar o HTML usando OWASP AntiSamy
        try {
            CleanResults cleanResults = antiSamy.scan(input);
            return cleanResults.getCleanHTML();
        } catch (Exception e) {
            throw new IllegalArgumentException("Erro ao sanitizar HTML", e);
        }
    }
}

