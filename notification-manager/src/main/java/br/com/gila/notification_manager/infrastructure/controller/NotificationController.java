package br.com.gila.notification_manager.infrastructure.controller;

import br.com.gila.notification_manager.core.service.NotificationService;
import br.com.gila.notification_manager.domain.notification.Notification;
import br.com.gila.notification_manager.domain.notification.NotificationRequest;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequestMapping(value = "api/v1")
public class NotificationController {

    @Autowired
    private NotificationService<Notification> notificationService;

    @Operation(summary = "Create a new notification")
    @PostMapping("/notifications")
    public ResponseEntity<?> sendNotification(@RequestBody NotificationRequest input) {
        try {
            notificationService.sendNotification(input);
        } catch (Exception e) {
            return ResponseEntity.internalServerError().body(e.getMessage());
        }

        return ResponseEntity.created(URI.create("/notifications")).body("");

    }

    @Operation(summary = "Retrieve notifications")
    @GetMapping("/notifications")
    public ResponseEntity<?> retrieveNotifications() {
        return ResponseEntity.ok(notificationService.retrieveNotifications());
    }

}