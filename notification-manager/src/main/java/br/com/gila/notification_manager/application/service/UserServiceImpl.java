package br.com.gila.notification_manager.application.service;

import br.com.gila.notification_manager.core.service.UserService;
import br.com.gila.notification_manager.core.type.CategoryType;
import br.com.gila.notification_manager.domain.user.User;
import br.com.gila.notification_manager.infrastructure.client.UserClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserClient client;

    public List<User> getUsersByCategory(List<CategoryType> categories) {
        return client.retrieveUsersBySubscribedCategories(categories);
    }

}
