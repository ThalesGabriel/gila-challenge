package br.com.gila.notification_manager.core.gateway;

import br.com.gila.notification_manager.core.domain.notification.NotificationDomain;

public interface ChannelGateway<T extends NotificationDomain> {
    void sendNotification(T notification);
}

