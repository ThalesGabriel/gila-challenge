package br.com.gila.notification_manager.core.domain;

public abstract class Validator <T extends Aggregate> {
    public abstract void validate(T object);
}

