package br.com.gila.notification_manager.core.gateway;

import br.com.gila.notification_manager.core.domain.notification.NotificationDomain;
import br.com.gila.notification_manager.domain.notification.Notification;

import java.util.List;

public interface NotificationGateway<T extends Notification> {

    void persist(NotificationDomain notification);
    List<T> retrieve();

}
