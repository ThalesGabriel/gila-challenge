package br.com.gila.notification_manager.core.domain;

public abstract class Aggregate<Id> extends Entity<Id> {


    protected Aggregate(Id id) {
        super(id);
    }

    public abstract void validate();


}
