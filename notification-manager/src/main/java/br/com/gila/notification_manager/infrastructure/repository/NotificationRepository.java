package br.com.gila.notification_manager.infrastructure.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.UUID;

public interface NotificationRepository extends MongoRepository<NotificationEntity, UUID> {
    List<NotificationEntity> findAllByOrderByCreatedAtDesc();
}
