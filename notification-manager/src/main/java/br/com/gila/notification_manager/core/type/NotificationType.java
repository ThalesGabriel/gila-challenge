package br.com.gila.notification_manager.core.type;

public enum NotificationType {
    SMS, EMAIL, PUSH_NOTIFICATION;

}
