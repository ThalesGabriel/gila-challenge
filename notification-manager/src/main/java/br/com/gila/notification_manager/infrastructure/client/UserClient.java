package br.com.gila.notification_manager.infrastructure.client;

import br.com.gila.notification_manager.core.type.CategoryType;
import br.com.gila.notification_manager.domain.user.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "user-client", url = "${spring.integrations.user-client.url}")
public interface UserClient {

    @GetMapping("/api/v1/users")
    List<User> retrieveUsersBySubscribedCategories(@RequestParam List<CategoryType> categories);

}
