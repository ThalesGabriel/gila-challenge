package br.com.gila.notification_manager.application.service;

import br.com.gila.notification_manager.application.channel.ChannelHandler;
import br.com.gila.notification_manager.core.domain.notification.NotificationRequestDomain;
import br.com.gila.notification_manager.core.gateway.NotificationGateway;
import br.com.gila.notification_manager.core.service.NotificationService;
import br.com.gila.notification_manager.core.service.UserService;
import br.com.gila.notification_manager.domain.notification.Notification;
import br.com.gila.notification_manager.domain.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NotificationServiceImpl implements NotificationService<Notification> {

    @Autowired
    private ChannelHandler firstHandlerProcessor;

    @Autowired
    private UserService userService;

    @Autowired
    private NotificationGateway<Notification> notificationGateway;

    @Override
    public void sendNotification(NotificationRequestDomain input) {
        List<User> users = userService.getUsersByCategory(input.getCategories());
        if(users.isEmpty())
            return;

        users.forEach(user -> firstHandlerProcessor.handle(input, user));
    }

    @Override
    public List<Notification> retrieveNotifications() {
        return notificationGateway.retrieve();
    }


}
