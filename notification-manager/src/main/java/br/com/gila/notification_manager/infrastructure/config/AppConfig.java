package br.com.gila.notification_manager.infrastructure.config;

import br.com.gila.notification_manager.application.channel.ChannelHandler;
import br.com.gila.notification_manager.application.channel.EmailHandler;
import br.com.gila.notification_manager.application.channel.PushHandler;
import br.com.gila.notification_manager.application.channel.SmsHandler;
import br.com.gila.notification_manager.infrastructure.adapter.sender.GenericSenderGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Autowired
    private GenericSenderGateway genericSenderGateway;

    @Bean
    public ChannelHandler firstHandlerProcessor() {
        ChannelHandler emailHandlerProcessor = new EmailHandler(genericSenderGateway);
        ChannelHandler smsHandlerProcessor = new SmsHandler(genericSenderGateway);
        ChannelHandler pushHandlerProcessor = new PushHandler(genericSenderGateway);

        emailHandlerProcessor.setNextHandler(smsHandlerProcessor);
        smsHandlerProcessor.setNextHandler(pushHandlerProcessor);
        return emailHandlerProcessor;
    }

}
