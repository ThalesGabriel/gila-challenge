package br.com.gila.notification_manager.infrastructure.adapter.database;

import br.com.gila.notification_manager.core.domain.notification.NotificationDomain;
import br.com.gila.notification_manager.core.gateway.NotificationGateway;
import br.com.gila.notification_manager.domain.notification.Notification;
import br.com.gila.notification_manager.infrastructure.repository.NotificationEntity;
import br.com.gila.notification_manager.infrastructure.repository.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MongoNotificationGateway implements NotificationGateway<Notification> {

    @Autowired
    private NotificationRepository repository;

    @Override
    public void persist(NotificationDomain notification) {
        this.repository.save(NotificationEntity.fromDomainObject(notification));
    }

    @Override
    public List<Notification> retrieve() {
        return repository.findAllByOrderByCreatedAtDesc().stream().map(Notification::fromEntity).toList();
    }

}
