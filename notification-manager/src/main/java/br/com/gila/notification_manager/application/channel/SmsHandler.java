package br.com.gila.notification_manager.application.channel;

import br.com.gila.notification_manager.core.domain.notification.NotificationRequestDomain;
import br.com.gila.notification_manager.core.gateway.ChannelGateway;
import br.com.gila.notification_manager.core.type.NotificationType;
import br.com.gila.notification_manager.domain.notification.Notification;
import br.com.gila.notification_manager.domain.user.User;

import java.util.List;

public class SmsHandler extends ChannelHandler {

    public SmsHandler(ChannelGateway<Notification> channelGateway) {
        super(channelGateway);
    }

    @Override
    protected boolean canHandle(List<NotificationType> subscribedChannels) {
        return subscribedChannels.contains(NotificationType.SMS);
    }

    @Override
    protected void processNotification(NotificationRequestDomain request, User user) {
        System.out.println("Processing SMS notification for user: " + user.getName());
        Notification notification = new Notification(NotificationType.SMS, user, request);
        channelGateway.sendNotification(notification);
    }

}
