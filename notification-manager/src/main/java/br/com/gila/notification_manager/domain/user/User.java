package br.com.gila.notification_manager.domain.user;

import br.com.gila.notification_manager.core.domain.Aggregate;
import br.com.gila.notification_manager.core.domain.user.UserDomain;
import br.com.gila.notification_manager.core.type.CategoryType;
import br.com.gila.notification_manager.core.type.NotificationType;

import java.util.List;
import java.util.UUID;

public class User extends Aggregate<UUID> implements UserDomain {
    private String phone;
    private String email;
    private String name;
    private List<CategoryType> subscribedCategories;
    private List<NotificationType> subscribedChannels;

    public User(UUID id, String phone, String email, String name, List<CategoryType> subscribedCategories, List<NotificationType> subscribedChannels) {
        super(id);
        this.phone = phone;
        this.email = email;
        this.name = name;
        this.subscribedCategories = subscribedCategories;
        this.subscribedChannels = subscribedChannels;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public List<CategoryType> getSubscribedCategories() {
        return subscribedCategories;
    }

    public List<NotificationType> getSubscribedChannels() {
        return subscribedChannels;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + this.id +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", subscribedCategories=" + subscribedCategories +
                ", subscribedChannels=" + subscribedChannels +
                '}';
    }

    @Override
    public void validate() {

    }
}

