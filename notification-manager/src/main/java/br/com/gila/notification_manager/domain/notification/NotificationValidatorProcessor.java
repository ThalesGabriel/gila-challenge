package br.com.gila.notification_manager.domain.notification;

import br.com.gila.notification_manager.core.domain.notification.NotificationValidator;
import br.com.gila.notification_manager.core.validation.TextInputValidation;
import br.com.gila.notification_manager.infrastructure.adapter.owasp.OwaspTextInputValidator;

import java.util.regex.Pattern;

public class NotificationValidatorProcessor extends NotificationValidator {

    private final TextInputValidation textInputValidation;

    public NotificationValidatorProcessor() {
        var owaspValidator = new OwaspTextInputValidator();
        owaspValidator.initialize();
        this.textInputValidation = owaspValidator;
    }

    @Override
    public void validate(Notification object) {
        validateMessage(object);
    }

    private void validateMessage(Notification notification) {
        if (textInputValidation == null) {
            throw new IllegalStateException("TextInputValidation dependency not injected");
        }

        var categories = notification.getNotificationRequest().getCategories();
        var message = notification.getNotificationRequest().getMessage();

        if (categories == null || categories.isEmpty()) {
            throw new IllegalArgumentException("The category list cannot be null or empty.");
        }

        if (message == null || message.isEmpty()) {
            throw new IllegalArgumentException("The message cannot be null or empty.");
        }

        String[] forbiddenPatterns = {"<script>", "javascript:"};
        for (String pattern : forbiddenPatterns) {
            if (Pattern.compile(pattern, Pattern.CASE_INSENSITIVE).matcher(message).find()) {
                throw new IllegalArgumentException("Prohibited pattern found in message: " + pattern);
            }
        }

        String sanitizedMessage = textInputValidation.validateAndSanitizeInput(message);
        if (!message.equals(sanitizedMessage)) {
            throw new IllegalArgumentException("The message contains prohibited characters.");
        }

        notification.sanitizeMessage(sanitizedMessage);
    }

}
