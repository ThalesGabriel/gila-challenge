package br.com.gila.notification_manager.infrastructure.repository;

import br.com.gila.notification_manager.core.domain.notification.NotificationDomain;
import br.com.gila.notification_manager.core.domain.notification.NotificationRequestDomain;
import br.com.gila.notification_manager.core.domain.user.UserDomain;
import br.com.gila.notification_manager.core.type.NotificationType;
import br.com.gila.notification_manager.core.type.StatusType;
import br.com.gila.notification_manager.domain.notification.NotificationRequest;
import br.com.gila.notification_manager.domain.user.User;
import nonapi.io.github.classgraph.json.Id;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.Instant;
import java.util.UUID;

@Document(collection = "notifications")
public class NotificationEntity implements NotificationDomain {

    @Id
    private UUID id;

    @Field("notification_request")
    private NotificationRequest notificationRequest;

    @Field("type")
    private NotificationType type;

    @Field("user")
    private User user;

    @Field("created_at")
    private Instant createdAt;

    @Field("status")
    private StatusType status;

    public NotificationEntity() {
    }

    public NotificationEntity(UUID id, NotificationType type, User user, Instant createdAt, StatusType status, NotificationRequest notificationRequest) {
        this.id = id;
        this.type = type;
        this.user = user;
        this.notificationRequest = notificationRequest;
        this.createdAt = createdAt;
        this.status = status;
    }

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public NotificationType getType() {
        return type;
    }

    @Override
    public UserDomain getUser() {
        return user;
    }

    @Override
    public NotificationRequestDomain getNotificationRequest() {
        return notificationRequest;
    }

    @Override
    public Instant getCreatedAt() {
        return createdAt;
    }

    @Override
    public StatusType getStatus() {
        return status;
    }

    public static NotificationEntity fromDomainObject(NotificationDomain notification) {
        return new NotificationEntity(notification.getId(), notification.getType(), (User) notification.getUser(), notification.getCreatedAt(), notification.getStatus(), (NotificationRequest) notification.getNotificationRequest());
    }
}
