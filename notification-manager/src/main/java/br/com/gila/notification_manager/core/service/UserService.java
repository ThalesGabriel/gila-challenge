package br.com.gila.notification_manager.core.service;

import br.com.gila.notification_manager.core.type.CategoryType;
import br.com.gila.notification_manager.domain.user.User;

import java.util.List;

public interface UserService {

    List<User> getUsersByCategory(List<CategoryType> categories);

}
