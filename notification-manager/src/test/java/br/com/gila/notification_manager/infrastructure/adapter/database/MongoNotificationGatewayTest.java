package br.com.gila.notification_manager.infrastructure.adapter.database;

import br.com.gila.notification_manager.core.type.CategoryType;
import br.com.gila.notification_manager.core.type.NotificationType;
import br.com.gila.notification_manager.core.type.StatusType;
import br.com.gila.notification_manager.domain.notification.Notification;
import br.com.gila.notification_manager.domain.notification.NotificationRequest;
import br.com.gila.notification_manager.domain.user.User;
import br.com.gila.notification_manager.infrastructure.repository.NotificationEntity;
import br.com.gila.notification_manager.infrastructure.repository.NotificationRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class MongoNotificationGatewayTest {

    @Mock
    private NotificationRepository repository;

    @InjectMocks
    private MongoNotificationGateway mongoNotificationGateway;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testPersist() {
        // Criar uma instância de NotificationRequest
        NotificationRequest notificationRequest = new NotificationRequest(
                Arrays.asList(CategoryType.SPORTS), "Test message"
        );

        // Criar uma instância de User
        User user = new User(UUID.randomUUID(), "1234567890", "user@example.com", "Test User",
                Arrays.asList(CategoryType.SPORTS), Arrays.asList(NotificationType.EMAIL));

        // Criar uma instância de Notification
        Notification notification = new Notification(
                NotificationType.EMAIL, user, notificationRequest
        );

        // Chamar o método persist
        mongoNotificationGateway.persist(notification);

        // Verificar se o método save foi chamado com o NotificationEntity correto
        verify(repository, times(1)).save(any(NotificationEntity.class));
    }

    @Test
    public void testRetrieve() {
        // Criação de uma lista de NotificationEntity simulada
        List<NotificationEntity> entities = Arrays.asList(
                new NotificationEntity(
                        UUID.randomUUID(),
                        NotificationType.EMAIL,
                        new User(UUID.randomUUID(), "1234567890", "user1@example.com", "User One",
                                Arrays.asList(CategoryType.SPORTS), Arrays.asList(NotificationType.EMAIL)),
                        Instant.now(),
                        StatusType.NOT_SENT,
                        new NotificationRequest(Arrays.asList(CategoryType.SPORTS), "Test message")
                ),
                new NotificationEntity(
                        UUID.randomUUID(),
                        NotificationType.SMS,
                        new User(UUID.randomUUID(), "0987654321", "user2@example.com", "User Two",
                                Arrays.asList(CategoryType.NEWS), Arrays.asList(NotificationType.SMS)),
                        Instant.now().minusSeconds(3600),
                        StatusType.SENT,
                        new NotificationRequest(Arrays.asList(CategoryType.NEWS), "Another test message")
                )
        );

        // Configurar o mock para retornar a lista de entidades
        when(repository.findAllByOrderByCreatedAtDesc()).thenReturn(entities);

        // Chamar o método retrieve
        List<Notification> notifications = mongoNotificationGateway.retrieve();

        // Verificar se o número de notificações retornadas é igual ao número de entidades
        assertEquals(entities.size(), notifications.size());

        // Verificar se os campos das notificações correspondem aos campos das entidades
        for (int i = 0; i < entities.size(); i++) {
            NotificationEntity entity = entities.get(i);
            Notification notification = notifications.get(i);
            assertEquals(entity.getId(), notification.getId());
            assertEquals(entity.getType(), notification.getType());
            assertEquals(entity.getCreatedAt(), notification.getCreatedAt());
            assertEquals(entity.getStatus(), notification.getStatus());
            assertEquals(entity.getNotificationRequest().getMessage(), notification.getNotificationRequest().getMessage());
            assertEquals(entity.getNotificationRequest().getCategories(), notification.getNotificationRequest().getCategories());
            assertEquals(entity.getUser().getId(), notification.getUser().getId());
            assertEquals(entity.getUser().getEmail(), notification.getUser().getEmail());
            assertEquals(entity.getUser().getPhone(), notification.getUser().getPhone());
            assertEquals(entity.getUser().getName(), notification.getUser().getName());
        }
    }
}
