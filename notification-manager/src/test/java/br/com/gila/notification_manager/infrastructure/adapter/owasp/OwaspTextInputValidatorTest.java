package br.com.gila.notification_manager.infrastructure.adapter.owasp;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class OwaspTextInputValidatorTest {

    private OwaspTextInputValidator validator;

    @BeforeEach
    public void setUp() {
        validator = new OwaspTextInputValidator();
    }

    @Test
    public void testInitialize() {
        assertDoesNotThrow(() -> validator.initialize(), "Initialization should not throw any exception");
    }

    @Test
    public void testValidateAndSanitizeInput_LengthExceedsLimit() {
        String input = "A".repeat(501); // Input length exceeds the limit of 500
        assertThrows(IllegalArgumentException.class, () -> validator.validateAndSanitizeInput(input),
                "Should throw IllegalArgumentException for input length exceeding limit");
    }

    @Test
    public void testValidateAndSanitizeInput_ForbiddenPatternFound() {
        String input = "<script>alert('Hello');</script>"; // Input contains a forbidden pattern
        assertThrows(IllegalArgumentException.class, () -> validator.validateAndSanitizeInput(input),
                "Should throw IllegalArgumentException for forbidden pattern found");
    }

}
