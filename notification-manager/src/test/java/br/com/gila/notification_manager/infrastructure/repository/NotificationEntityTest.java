package br.com.gila.notification_manager.infrastructure.repository;

import br.com.gila.notification_manager.core.type.CategoryType;
import br.com.gila.notification_manager.core.type.NotificationType;
import br.com.gila.notification_manager.domain.notification.Notification;
import br.com.gila.notification_manager.domain.notification.NotificationRequest;
import br.com.gila.notification_manager.domain.user.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class NotificationEntityTest {

    private Notification notification;
    private NotificationEntity notificationEntity;

    @BeforeEach
    public void setUp() {
        // Criar instâncias de NotificationRequest e User
        NotificationRequest notificationRequest = new NotificationRequest(
                Arrays.asList(CategoryType.SPORTS), "Test message"
        );

        User user = new User(UUID.randomUUID(), "1234567890", "user@example.com", "Test User",
                Arrays.asList(CategoryType.SPORTS), Arrays.asList(NotificationType.EMAIL));

        // Criar uma instância de Notification
        notification = new Notification(
                NotificationType.EMAIL, user, notificationRequest
        );

        // Inicializar NotificationEntity a partir da Notification
        notificationEntity = NotificationEntity.fromDomainObject(notification);
    }

    @Test
    public void testNotificationEntityConstruction() {
        // Verificar os campos básicos
        assertNotNull(notificationEntity);
        assertEquals(notification.getId(), notificationEntity.getId());
        assertEquals(notification.getType(), notificationEntity.getType());
        assertEquals(notification.getCreatedAt(), notificationEntity.getCreatedAt());
        assertEquals(notification.getStatus(), notificationEntity.getStatus());

        // Verificar o campo de User
        assertEquals(notification.getUser().getId(), notificationEntity.getUser().getId());
        assertEquals(notification.getUser().getEmail(), notificationEntity.getUser().getEmail());
        assertEquals(notification.getUser().getPhone(), notificationEntity.getUser().getPhone());
        assertEquals(notification.getUser().getName(), notificationEntity.getUser().getName());

        // Verificar o campo de NotificationRequest
        assertEquals(notification.getNotificationRequest().getCategories(), notificationEntity.getNotificationRequest().getCategories());
        assertEquals(notification.getNotificationRequest().getMessage(), notificationEntity.getNotificationRequest().getMessage());
    }

    @Test
    public void testGetters() {
        // Verificar se os getters retornam os valores corretos
        assertEquals(notification.getId(), notificationEntity.getId());
        assertEquals(notification.getType(), notificationEntity.getType());
        assertEquals(notification.getCreatedAt(), notificationEntity.getCreatedAt());
        assertEquals(notification.getStatus(), notificationEntity.getStatus());

        // Verificar os campos do User retornados pelos getters
        assertEquals(notification.getUser().getId(), notificationEntity.getUser().getId());
        assertEquals(notification.getUser().getEmail(), notificationEntity.getUser().getEmail());
        assertEquals(notification.getUser().getPhone(), notificationEntity.getUser().getPhone());
        assertEquals(notification.getUser().getName(), notificationEntity.getUser().getName());

        // Verificar os campos do NotificationRequest retornados pelos getters
        assertEquals(notification.getNotificationRequest().getCategories(), notificationEntity.getNotificationRequest().getCategories());
        assertEquals(notification.getNotificationRequest().getMessage(), notificationEntity.getNotificationRequest().getMessage());
    }

    @Test
    public void testFromDomainObject() {
        // Testar a criação de NotificationEntity a partir de NotificationDomain
        NotificationEntity entity = NotificationEntity.fromDomainObject(notification);

        // Verificar se os campos correspondem corretamente
        assertEquals(notification.getId(), entity.getId());
        assertEquals(notification.getType(), entity.getType());
        assertEquals(notification.getCreatedAt(), entity.getCreatedAt());
        assertEquals(notification.getStatus(), entity.getStatus());

        // Verificar os campos do User
        assertEquals(notification.getUser().getId(), entity.getUser().getId());
        assertEquals(notification.getUser().getEmail(), entity.getUser().getEmail());
        assertEquals(notification.getUser().getPhone(), entity.getUser().getPhone());
        assertEquals(notification.getUser().getName(), entity.getUser().getName());

        // Verificar os campos do NotificationRequest
        assertEquals(notification.getNotificationRequest().getCategories(), entity.getNotificationRequest().getCategories());
        assertEquals(notification.getNotificationRequest().getMessage(), entity.getNotificationRequest().getMessage());
    }
}

