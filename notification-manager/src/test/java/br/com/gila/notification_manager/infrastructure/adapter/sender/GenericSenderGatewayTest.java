package br.com.gila.notification_manager.infrastructure.adapter.sender;

import br.com.gila.notification_manager.core.domain.user.UserDomain;
import br.com.gila.notification_manager.core.gateway.NotificationGateway;
import br.com.gila.notification_manager.core.type.CategoryType;
import br.com.gila.notification_manager.core.type.NotificationType;
import br.com.gila.notification_manager.core.type.StatusType;
import br.com.gila.notification_manager.domain.notification.Notification;
import br.com.gila.notification_manager.domain.notification.NotificationRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

public class GenericSenderGatewayTest {

    @Mock
    private NotificationGateway<Notification> notificationGateway;

    @InjectMocks
    private GenericSenderGateway genericSenderGateway;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSendNotification() {
        // Criação de mocks para UserDomain e NotificationRequest
        UserDomain mockUser = mock(UserDomain.class);
        NotificationRequest mockRequest = new NotificationRequest(
                List.of(CategoryType.NEWS), "Test message"
        );

        // Criar uma instância de Notification
        Notification notification = new Notification(NotificationType.EMAIL, mockUser, mockRequest);

        // Chamar o método sendNotification
        genericSenderGateway.sendNotification(notification);

        // Verificar se o método markMessageAsSent foi chamado
        assert(notification.getStatus() == StatusType.SENT);

        // Verificar se o método persist foi chamado com a notificação atualizada
        verify(notificationGateway, times(1)).persist(notification);
    }
}
