package br.com.gila.user_config.infrastructure.repository;

import br.com.gila.user_config.core.gateway.UserGateway;
import br.com.gila.user_config.core.type.CategoryType;
import br.com.gila.user_config.core.type.NotificationType;
import br.com.gila.user_config.domain.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
public class UserMockRepository implements UserGateway {
    private List<User> users;

    public UserMockRepository() {
        users = List.of(
                new User(UUID.randomUUID(), "1234567890", "user1@example.com", "User One", List.of(CategoryType.SPORTS, CategoryType.FINANCE), List.of(NotificationType.EMAIL, NotificationType.SMS)),
                new User(UUID.randomUUID(), "0987654321", "user2@example.com", "User Two", List.of(CategoryType.FINANCE, CategoryType.NEWS), List.of(NotificationType.PUSH_NOTIFICATION, NotificationType.SMS)),
                new User(UUID.randomUUID(), "1122334455", "user3@example.com", "User Three", List.of(CategoryType.FINANCE), List.of(NotificationType.EMAIL)),
                new User(UUID.randomUUID(), "5566778899", "user4@example.com", "User Four", List.of(CategoryType.SPORTS, CategoryType.FINANCE), List.of(NotificationType.PUSH_NOTIFICATION)),
                new User(UUID.randomUUID(), "6677889900", "user5@example.com", "User Five", List.of(CategoryType.NEWS), List.of(NotificationType.EMAIL, NotificationType.PUSH_NOTIFICATION))
        );
    }

    @Override
    public List<User> getUsersByCategory(List<CategoryType> categories) {
        List<User> usersInCategory = new ArrayList<>();

        for (User user : users) {
            if (user.getSubscribedCategories().stream().anyMatch(categories::contains)) {
                usersInCategory.add(user);
            }
        }

        return usersInCategory;
    }
}