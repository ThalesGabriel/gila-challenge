package br.com.gila.user_config.infrastructure.api;

import br.com.gila.user_config.core.service.UserService;
import br.com.gila.user_config.core.type.CategoryType;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "api/v1")
public class UserController {

    @Autowired
    private UserService userService;

    @Operation(summary = "Retrieve users by their subscribed categories")
    @GetMapping("/users")
    public ResponseEntity<?> retrieveUsersBySubscribedCategories(@RequestParam List<CategoryType> categories) {
        return ResponseEntity.ok(userService.getUsersByCategory(categories));
    }

}
