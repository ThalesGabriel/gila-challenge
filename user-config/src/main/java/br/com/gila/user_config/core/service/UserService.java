package br.com.gila.user_config.core.service;

import br.com.gila.user_config.core.type.CategoryType;
import br.com.gila.user_config.domain.User;

import java.util.List;

public interface UserService {

    List<User> getUsersByCategory(List<CategoryType> categories);

}
