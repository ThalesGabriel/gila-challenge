package br.com.gila.user_config.core.type;

public enum CategoryType {
    SPORTS, NEWS, FINANCE;
}
