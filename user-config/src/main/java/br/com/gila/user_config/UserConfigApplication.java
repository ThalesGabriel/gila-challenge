package br.com.gila.user_config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserConfigApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserConfigApplication.class, args);
	}

}
