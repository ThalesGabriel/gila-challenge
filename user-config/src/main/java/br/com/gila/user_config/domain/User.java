package br.com.gila.user_config.domain;

import br.com.gila.user_config.core.type.CategoryType;
import br.com.gila.user_config.core.type.NotificationType;

import java.util.List;
import java.util.UUID;

public class User {
    private UUID id;
    private String phone;
    private String email;
    private String name;
    private List<CategoryType> subscribedCategories;
    private List<NotificationType> subscribedChannels;

    public User(UUID id, String phone, String email, String name, List<CategoryType> subscribedCategories, List<NotificationType> subscribedChannels) {
        this.id = id;
        this.phone = phone;
        this.email = email;
        this.name = name;
        this.subscribedCategories = subscribedCategories;
        this.subscribedChannels = subscribedChannels;
    }

    public UUID getId() {
        return id;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public List<CategoryType> getSubscribedCategories() {
        return subscribedCategories;
    }

    public List<NotificationType> getSubscribedChannels() {
        return subscribedChannels;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + id +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", subscribedCategories=" + subscribedCategories +
                ", subscribedChannels=" + subscribedChannels +
                '}';
    }
}

