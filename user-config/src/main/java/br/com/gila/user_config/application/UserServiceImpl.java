package br.com.gila.user_config.application;

import br.com.gila.user_config.core.gateway.UserGateway;
import br.com.gila.user_config.core.service.UserService;
import br.com.gila.user_config.core.type.CategoryType;
import br.com.gila.user_config.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserGateway userGateway;

    public List<User> getUsersByCategory(List<CategoryType> categories) {
        return userGateway.getUsersByCategory(categories);
    }

}
