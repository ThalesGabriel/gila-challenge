package br.com.gila.user_config.core.gateway;

import br.com.gila.user_config.core.type.CategoryType;
import br.com.gila.user_config.domain.User;

import java.util.List;

public interface UserGateway {

    List<User> getUsersByCategory(List<CategoryType> categories);

}
