package br.com.gila.user_config.core.type;

public enum NotificationType {
    SMS, EMAIL, PUSH_NOTIFICATION;

}
