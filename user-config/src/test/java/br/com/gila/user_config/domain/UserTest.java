package br.com.gila.user_config.domain;

import br.com.gila.user_config.core.type.CategoryType;
import br.com.gila.user_config.core.type.NotificationType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class UserTest {

    private UUID userId;
    private String phone;
    private String email;
    private String name;
    private List<CategoryType> subscribedCategories;
    private List<NotificationType> subscribedChannels;
    private User user;

    @BeforeEach
    public void setUp() {
        // Inicializa os dados do usuário para os testes
        userId = UUID.randomUUID();
        phone = "1234567890";
        email = "user@example.com";
        name = "User Example";
        subscribedCategories = Arrays.asList(CategoryType.SPORTS, CategoryType.FINANCE);
        subscribedChannels = Arrays.asList(NotificationType.EMAIL, NotificationType.SMS);

        // Cria uma instância do User com os dados de teste
        user = new User(userId, phone, email, name, subscribedCategories, subscribedChannels);
    }

    @Test
    public void testGetId() {
        assertEquals(userId, user.getId());
    }

    @Test
    public void testGetPhone() {
        assertEquals(phone, user.getPhone());
    }

    @Test
    public void testGetEmail() {
        assertEquals(email, user.getEmail());
    }

    @Test
    public void testGetName() {
        assertEquals(name, user.getName());
    }

    @Test
    public void testGetSubscribedCategories() {
        assertEquals(subscribedCategories, user.getSubscribedCategories());
    }

    @Test
    public void testGetSubscribedChannels() {
        assertEquals(subscribedChannels, user.getSubscribedChannels());
    }

    @Test
    public void testToString() {
        String expectedString = "User{" +
                "userId=" + userId +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", subscribedCategories=" + subscribedCategories +
                ", subscribedChannels=" + subscribedChannels +
                '}';
        assertEquals(expectedString, user.toString());
    }

    @Test
    public void testUserNotNull() {
        // Verifica se a instância do usuário não é nula
        assertNotNull(user);
    }
}
