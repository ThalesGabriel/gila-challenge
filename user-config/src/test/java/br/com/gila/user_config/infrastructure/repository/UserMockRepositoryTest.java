package br.com.gila.user_config.infrastructure.repository;

import br.com.gila.user_config.core.type.CategoryType;
import br.com.gila.user_config.domain.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UserMockRepositoryTest {

    private UserMockRepository userRepository;

    @BeforeEach
    public void setUp() {
        userRepository = new UserMockRepository();
    }

    @Test
    public void testGetUsersByCategory() {
        List<CategoryType> categories = Arrays.asList(CategoryType.SPORTS, CategoryType.NEWS);
        List<User> usersInCategories = userRepository.getUsersByCategory(categories);

        // Verify that the correct number of users is returned
        assertEquals(4, usersInCategories.size());

        // Verify that each user returned belongs to at least one of the specified categories
        for (User user : usersInCategories) {
            boolean isInCategory = user.getSubscribedCategories().stream().anyMatch(categories::contains);
            assertTrue(isInCategory);
        }
    }

    @Test
    public void testGetUsersByNonExistingCategory() {
        // Test with a category that doesn't exist in the mock data
        List<CategoryType> categories = new ArrayList<>();
        List<User> usersInCategories = userRepository.getUsersByCategory(categories);

        // No users should be returned for non-existing categories
        assertEquals(0, usersInCategories.size());
    }
}
