<h1 style="text-align:center;">Notification Test</h1>

<h3>This project implements a notification management service, offering functionalities to send and retrieve notifications.</h3>

<hr/>

<br/>

<h3>Modules</h3>

<ul>
    <li><strong>Notification Manager API:</strong> Contains the REST API for notification management.</li>
    <li><strong>User config API:</strong> Simple mocked REST API to retrieve user configurations.</li>
    <li><strong>Frontend:</strong> Simple frontend built to consume Notification Test.</li>
</ul>

<hr/>

<h3>How to Run the Solution</h3>

<ol>
    <li>Clone the repository</li>
    <li>Navigate to the cloned project folder</li>
    <li>Make sure you have Docker and Docker Compose installed on your machine</li>
    <li>Run <code>docker-compose up -d</code></li>
    <li>Open your favorite browser at <a>http://localhost:3000</a></li>
</ol>

<h3>Run Simple Tests</h3>

<ol>
    <li>Open your preferred browser</li>
    <li>Open three tabs for each service:
        <ul>
            <li>User config -> <a href="http://localhost:8081/swagger-ui/index.html">http://localhost:8081/swagger-ui/index.html</a></li>
            <li>Notifications manager -> <a href="http://localhost:8080/swagger-ui/index.html">http://localhost:8080/swagger-ui/index.html</a></li>
        </ul>
    </li>
</ol>

<h3>Example Response</h3>

<p>An example request and response are shown below:</p>

<p><strong>POST /notifications</strong></p>
<pre>
{
  "categories": ["sports", "news"],
  "message": "Hello world!"
}
</pre>

<p>It will not respond, but you can retrieve all notifications at the specific endpoint:</p>
<pre>
[
  {
    "id": "8bac3d6d-366d-4acd-9853-eb4bbac2e5be",
    "type": "PUSH_NOTIFICATION",
    "user": {
      "id": "367f0350-ba35-4031-b7a9-bad75f55c091",
      "phone": "6677889900",
      "email": "user5@example.com",
      "name": "User Five",
      "subscribedCategories": [
        "NEWS"
      ],
      "subscribedChannels": [
        "EMAIL",
        "PUSH_NOTIFICATION"
      ]
    },
    "notificationRequest": {
      "categories": [
        "NEWS",
        "SPORTS"
      ],
      "message": "Super notícia legal"
    },
    "createdAt": "2024-06-10T07:24:33.054Z",
    "status": "SENT"
  },
  ...
]
</pre>

<h3>Project Features</h3>

<ul>
    <li>Developed using Spring Boot as the Java framework.</li>
    <li>API documentation using Swagger with OpenAPI Specification.</li>
    <li>Docker configuration for easy deployment.</li>
    <li>Utilization of Mongo as the database for notification manager.</li>
    <li>Unit tests implemented with JUnit and Mockito.</li>
</ul>

<h3>Shutdown</h3>

<ul>
    <li>Run <code>docker-compose down --volumes</code></li>
</ul>
